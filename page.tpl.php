<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
<head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
</head>
<body>
<!-- PRIMARY_LINKS -->
	<div class="menuWrapper">
	<div class="menuBlockSub">
	<div id="menuItems">
		<?php if (is_array($primary_links)) : ?>
        <?php 
          foreach($primary_links AS $links){
            echo l($links['title'], $links['href']);
          }
          endif;
        ?>
	</div>
	</div>
	</div>
<!-- TOP -->
<div class="spotTopWrapperSubpage">
<div class="spotTopBlockSubpage">
<div class="baseArea">
<div class="banner">
		<a href="<?php print check_url($base_path); ?>"><?php
			if ($logo) {
              print '<img src="'. check_url($logo) .'" id="logo" />';
            }
        ?></a>
</div>
<div class="header">
<h2><a href="<?php print check_url($base_path); ?>"><?php print check_plain($site_name); ?></a></h2>
<p><?php print check_plain($site_slogan); ?></p>
</div>
</div>
</div>
</div>
<div class="spotMenuSeparatorWrapper"><div class="spotMenuSeparatorBlock"></div></div>
<!-- MENU -->
<div class="baseBlock">
<div class="menuleft">
  <?php if ($sidebar_left != ""): ?>
      <?php print $sidebar_left ?>
  <?php endif; ?> 
  
  <?php if ($sidebar_right != ""): ?>
      <?php print $sidebar_right ?>
    <?php endif; ?>
</div>
<!-- CONTENT -->
<div class="base">
<?php if ($breadcrumb): print $breadcrumb; endif; ?>
<?php print $header; ?>
<?php if ($title != ""): ?>
<h1><?php print $title ?></h1>
<?php endif; ?>
<?php if ($tabs != ""): ?>
<?php print $tabs ?>
<?php endif; ?>
<?php if ($mission != ""): ?>
<p id="mission"><?php print $mission ?></p>
<?php endif; ?>
<?php if ($help != ""): ?>
<p id="help"><?php print $help ?></p>
<?php endif; ?>
<?php if ($messages != ""): ?>
<div id="message"><?php print $messages ?></div>
<?php endif; ?>   
<!-- start main content -->
<?php print($content) ?>
<!-- end main content -->
</div>
<div class="columnClear"></div>
</div>
<!-- FOOTER -->
<div class="footerWrapper">
<div class="footerBlock">
<div class="footerbase">
<?php print $footer_message ?>
</div></div></div>
<div align="center" class="copy">
	<p>powered by <a href="http://www.drupal.org/">Drupal</a> - <!-- Please do not remove this command line --> template <a href="http://florall.ru/">FlorAll</a></p>
</div>
<?php print $closure;?>
</body>
</html>

